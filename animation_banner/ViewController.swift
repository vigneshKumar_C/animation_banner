//
//  ViewController.swift
//  animation_banner
//
//  Created by vignesh kumar c on 23/08/23.
//

import UIKit

class ViewController: UIViewController {
    var currentImageIndex = 0
    @IBOutlet weak var bannerImageView: UIImageView!
    var images = ["one", "two"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func clickMeTapped(_ sender: Any) {
       // bannerNotification(text: "Banner Test with lots of text to wrap onto multiple lines. We're using explict frames and calculating the height of the (potentially multiline) label.")
       
        startBannerAnimation()
    }
    
    func bannerNotification(text: String) {
        
        let container = UIView()
        
        let label = UILabel()
        
        container.addSubview(label)
        container.backgroundColor = UIColor(red: 0.0, green: 0.5, blue: 1.0, alpha: 1.0)
        label.backgroundColor = .yellow
        label.numberOfLines = 0
        label.font = UIFont(name:"Helvetica Neue", size: 16.0)
        label.text = text
        let hPadding: CGFloat = 16.0
        let vTopPadding: CGFloat = 32.0
        let vBotPadding: CGFloat = 16.0
        let width = UIScreen.main.bounds.width
        let w = UIApplication.shared.windows[0]
        w.addSubview(container)
        
        let labelSize = label.systemLayoutSizeFitting(CGSize(width: width - (hPadding * 2.0),
                                                             height: UIView.layoutFittingCompressedSize.height))
        
        var containerRect = CGRect.zero
        containerRect.size = CGSize(width: width, height: labelSize.height + vTopPadding + vBotPadding)
        container.frame = containerRect
        let labelRect = containerRect.inset(by: UIEdgeInsets(top: vTopPadding, left: hPadding, bottom: vBotPadding, right: hPadding))
        label.frame = labelRect
        container.frame.origin.y = -container.frame.size.height
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveLinear, animations: {
                container.frame.origin.y = 0
            }) { (finished) in
                UIView.animate(withDuration: 0.4,delay: 2.0, options: .curveLinear, animations: {
                    container.frame.origin.y = -container.frame.size.height
                })
            }
        }
        
    }

    func startBannerAnimation() {
        let totalBannerWidth = bannerImageView.image?.size.width
        bannerImageView.image = UIImage(named: self.images[self.currentImageIndex])
        self.currentImageIndex = (currentImageIndex + 1) % self.images.count
        bannerImageView.frame.origin.x = view.bounds.width
        UIView.animate(withDuration: 10.0, delay: 0, options: [.curveLinear, .repeat], animations: {
            self.bannerImageView.frame.origin.x = -(totalBannerWidth ?? 0.0)
        }, completion: nil)
    }
}
